#################################################################################
# PYTHON SOURCE MODEL                                                           #
# verSION 0.1 - 02/10/2019 - Christophe LIMA                                    #
#################################################################################
#                                                                               #
# Resolver.py - Resolver pour "concert à tout prix"                             #
# Exercice pour le cap@attack 2019                                              #
#                                                                               #
# version 0.1 - 26/09/2019 - version initiale                                   #
#                                                                               #
#################################################################################

#################################################################################
# import code                                                                   #
#################################################################################
import sys                # bib sytème ( accés fichiers ect... )
import math               # bib fonctions mathématiques ( sqrt, cos,etc... )
import copy               # bib utilitaire de copie des tableaux ( matrices )
import itertools
################################################################################
# variables maquettes                                                           #
#################################################################################
MQ_Debug = False            # active/désactive le debug mode
MQ_NivDebug = 9             # niveau debug 0 -> min 9 --> max 
MQ_START = "*START*"        # balise de début pour debug mode
MQ_END = "*END  *"            # balise de fin pour debug modei
MQ_NIV1 = "(n1)+" 
MQ_NIV2 = "   (n2)+" 
MQ_NIV3 = "      (n3)+" 
MQ_NIV4 = "         (n4)+" 
MQ_NIV5 = "            (n5)+" 
MQ_NIV6 = "               (n6)+"
MQ_SEPM = "=========================================================================="
MQ_SEPm = "--------------------------------------------------------------------------"
#################################################################################
# declarations des variables globales au scripts                                #
#################################################################################
MatriceConcert = []
MatriceConcertALL = []
doublon = []
privateMode = True
#################################################################################
# declarations des constantes                                                   #
#################################################################################
# learning to fly : int , str
#################################################################################
if privateMode != True:
    FILE_IN = "Files/in/public{id}.in"
    FILE_OUT = "Files/out/public{id}.out"
else:
    FILE_IN = "Files/in/private{id}.in"
    FILE_OUT = "Files/out/private{id}.out"

NB_TESTS_TO_SOLVE = 10 
i = 0
ind = 0
nbplanning = 0
nbnoplanning = 0

#################################################################################
# Algoritme principal                                                           #
#################################################################################
for i in range (NB_TESTS_TO_SOLVE):
    MatriceConcert[:] = []
    solution = None


#-------------------------------------------------------------------------------
#Open et lecture de la solution 
    if ( MQ_Debug == True ) & (MQ_NivDebug < 9 ):
        print(MQ_NIV2,FILE_OUT.format(id=i))

    with open(FILE_OUT.format(id=i), 'r') as f:
        sol =  f.read()
        solution =sol.split()

    if ( MQ_Debug == True ):
        print(MQ_NIV2,"+solution=",solution,"+ lecture+split SUCCESS")
    
#-------------------------------------------------------------------------------
#Open et lecture des données inputs
    if ( MQ_Debug == True ) & (MQ_NivDebug < 9 ):
        print(MQ_NIV2,FILE_IN.format(id=i),"+")
 
    with open(FILE_IN.format(id=i), 'r') as f:
        
        if ( MQ_Debug == True ):
            print(MQ_NIV2,"Open du fichier ", FILE_IN.format(id=i) ," SUCCESS")
            print(MQ_SEPm)
        
        lines = f.readlines()
        index = 0
        for line in lines:
            za , xa , ya  = map( str, line.split())
            MatriceConcert.append( [ za, xa , ya ] )
            index = index + 1
            
            if ( MQ_Debug == True ) & ( MQ_NivDebug < 8 ):
                print(MQ_NIV3,"Inputs ",za, "-", xa, "-", ya)
    
    NbPoints = index

    if (MQ_Debug == True):
        print(MQ_NIV2,"Nombre de concert " , index ) 
        print(MQ_SEPm)

#################################################################################
    # on utilise les fonctions du module itertools pour calculer l'ensemble
    # des solutions possibles 
    # --> utilsiation de la fonction permutations avec le nombre de tuples variant 
    # de 1 au nombre de concert total 

    if (MQ_Debug == True):
        print(MQ_NIV2,"itertools-permutation-")
       
    MatriceConcertALL[:] = []
    nbplanning = 0
    nbnoplanning = 0

    for ind in range(index):
        for sol in itertools.permutations( MatriceConcert, ind ):
            if ( MQ_Debug == True) & (MQ_NivDebug < 2) :
                print(MQ_NIV4, sol)
        
            hprev = ''
            for con in sol:
                if ( MQ_Debug == True ) & (MQ_NivDebug < 1 ):
                    print(MQ_NIV5,"con[1]", con[1], "hprev ", hprev)
            
            # on ne conserve QUE les solutions pour lesquelles les concerts
            # se suivent dans le temps 
                if con[1] >= hprev: 
                    hprev = con[2]
                else:
                    hprev = '99:99'
                    if ( MQ_Debug == True )& (MQ_NivDebug < 1 ):
                        print(MQ_NIV6,"NO ")

            if (hprev != '99:99') :
                MatriceConcertALL.append(sol)
                nbplanning = nbplanning + 1
                if ( MQ_Debug == True ) & ( MQ_NivDebug < 8 ) :
                    print(MQ_NIV5,"APPEND ", sol)
            else:
                nbnoplanning = nbnoplanning + 1
                if ( MQ_Debug == True ) & ( MQ_NivDebug < 4 ) :
                    print(MQ_NIV5,"<excl> ", sol)
                
    #MatriceConcertALL = compress( MatriceConcertALL , True , True  ) 
   
    if (MQ_Debug == True):
        print(MQ_NIV3,"Nombre de solution trouvées" , nbplanning, "* Nombre de solution exclues " , nbnoplanning ) 
        print(MQ_SEPm)
    
    if (MQ_Debug == True):
        print(MQ_SEPm)

#-------------------------------------------------------------------------------
# calcul du resultat
#-------------------------------------------------------------------------------
    # on balaye MatriceconcertALL qui contient l'ensemble des solution possible 
    # et on conserve celle qui comporte le plus de concert
    # production d'une erreur si plusieurs solutions sont possibles
    lng = 0
    Resultat = '<null>'
    doublon[:] = []

    for sol in MatriceConcertALL:
        if (MQ_Debug == True ) & (MQ_NivDebug < 8  ):
            print ( MQ_NIV3,"Sol x " , sol )

        if ( len(sol) >= lng ):

            Resultat = ''
            for con in sol:
                if Resultat == '':
                    Resultat = con[0]
                else:
                    Resultat = Resultat + '->' + con[0]

            if ( len(sol) > lng):
                doublon[:] = []
            else:
                doublon.append(Resultat)
            #    print("**WARN**Doublon",lng, "*", Resultat)

            #if ( len(sol) == lng ) :
            #    print ("**WARN**!!Doublon possible !!", lng , Resultat )
            
            lng = len(sol)

            if (MQ_Debug == True )& (MQ_NivDebug < 9  ):
                print ( MQ_NIV3,"SELECT ", lng , "|", Resultat, "|" , sol )

    if ( MQ_Debug== True ):
        print(MQ_NIV3,"FIN - Traitement première occurence ", ind  )
    
    ind = ind + 1

#-------------------------------------------------------------------------------
# Alimentation du resultat final a partir de l'ensemble des solutions possibles
#-------------------------------------------------------------------------------

    #Resultat = "<Null>"

#-------------------------------------------------------------------------------
# restitution et contrôle du resultat
#-------------------------------------------------------------------------------
    if ( MQ_Debug== True ):
        print(MQ_SEPm)
        print(MQ_NIV2,"Restitution du Resultat final " )

    if ( solution[0] == Resultat ):
        print("    OK: {}".format(solution))
    else:
        print(" >> KO: {} / {}".format(solution, Resultat))
    
    if len(doublon) > 1:
        print(MQ_SEPM)
        print("Exception ! DOUBLONS !! " )
        print( doublon )

    if ( MQ_Debug== True ):
        print(MQ_SEPM)
#
#################################################################################
# LEARNING TO FLY - PYTHON CODE EXAMPLES                     
#
#################################################################################
# Condition ---- IF -------------------------------------------------------------
#
#if (solution == Resultat):
#    something
#else:
#    something else
#
# boucle --- FOR ----------------------------------------------------------------
#
# for <condition>:
#     something -> la fin de l'indentation donne la fin de la boucle
#
# mise en forme - format --------------------------------------------------------
#
# print("    OK: {}".format(solution))
#
# matrice - copie compléte ( deepcopy )------------------------------------------
# MatriceCheminsTMP = copy.deepcopy(MatriceChemins)
#
# > fractionner un tableau --> .split()
#
# Fonction --- def---------------------------------------------------------------
# def nom_fonction(liste de paramètres):
#      bloc d'instructions
