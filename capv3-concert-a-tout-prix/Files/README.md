==========================================================================================
Court : concerts � tout prix
Sujet : Optimiser votre planning afin d'assister � un maximum de concert
--------------------------------------------------------------------
Apres des heures d'attentes vous avez r�ussi a avoir votre passe une journ�e pour le HellFest.
Cela a �t� une telle gal�re pour obtenir votre passe que vous voulez absolument passer cette journ�e a voir le plus grand nombre de concert possible.
Vous etes donc en train d'�tudier attentivement ls liste des concert pr�vus ce jour l� pour optimiser votre programme.

remarque : 
- le programme des concert est fourni sous la forme nom_du_groupe heure_de_debut heure_de_fin
- les horaires sont exprim�s au format hh:mm
- les noms de groupe ne comporte pas d'espace
-
par exemple : 
DivFist 09:00 11:30
Iron_Rider 14:30 16:00

-une r�ponse exacte est attendue : un trajet dont la longueur est 
proche du trajet optimal sera consid�r� comme faux. 
-Du fait de la contrainte pr�c�dente et afin de ne pas �x�cuter des calculs trop lourd, 
le programme est limit� � 6 concerts maximum.
-le r�sultat est attendu sous la forme : "nom_du_groupe_1->nom_du_groupe_2-->".... avec nom_du_groupe1 correspondant � votre premier concert. 


      
