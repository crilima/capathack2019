
Description courte : Old Rock Hero


Old Rock Hero
===================



Vous etes une rock-star plan�taire en cheveux blanc r�alisant avec son groupe l�gendaire une �ni�me tourn�e d'adieu.
Mais cette ann�e , madame Irmy vous demande expressement de r�aliser la tourn�e la courte possible afin d'�tre de retour � l'hospice tr�s rapidement.
  
 


##### <i class="icon-file"></i> 

Entr�es fournies :


Les coordonn�es que vous avez obtenus se pr�sentent sous la forme d'une liste de 2 chiffres entiers positifs ou n�gatifs s�par�s par un espace.

La premi�re ligne repr�sente vos coordonn�es actuelles et donc votre point de d�part. 

Chaque coordonn�es sera nomm�e en fonction de sa position dans la liste : 

p0 : point de d�part

p1 : coordonn�es du premier lieu de la liste ( 2�me ligne)

p2 : coordonn�es du second lieu de la liste (3�me ligne )

etc....



Exemple : 

...

999 10  
-25 0 

...
ou

...

1000 -15 

14 10 

-7 20

...


Afin de limiter le temps de calcul , la patrouille est limit� � 10 secteurs maximum. 



##### <i class="icon-file"></i> 

Sortie � fournir :


La r�ponse doit �tre donn�e sous la forme point de d�part->premier destination-->seconde destination-> etc..

sh: :wq: command not found

"p0->p5->p2->p1->p4"

ou

"p0->p3->p1->p2"


Nous attendons une r�ponse exacte : la d�termination du chemin le plus court devra �tre r�alis�e, si possible, en force brute. 







