#Resolver.py - CLI pour Old Rock hero le 26/09/2019
import sys
import math
import copy
MatricePoint = []
MatriceDistance = []
MatriceChemins = []

FILE_IN = "Files/in/public{id}.in"
FILE_OUT = "Files/out/public{id}.out"

NB_TESTS_TO_SOLVE = 10 
for i in range (NB_TESTS_TO_SOLVE):    
    MatricePoint[:] = []
    MatriceDistance[:] = []
    MatriceChemins[:] = []
    solution = None
    TailleMatriceDistance = 0
    NpPoints = 0

#Stage 1 - Open et lecture de la solution 
    with open(FILE_OUT.format(id=i), 'r') as f:
        sol =  f.read()
        solution =sol.split()

#Stage 2 - Open et lecture des données inputs  
    with open(FILE_IN.format(id=i), 'r') as f:
#        print("========================NEW FILE=================================")
#        print("=",i )
#        print("=================================================================")
        lines = f.readlines()
        index = 0
        for line in lines:
            xa , ya = map( int, line.split())
#            print("Inputs ", xa, "-", ya "-----------------------------------")
            MatricePoint.append( [ "p" + str(index) , xa , ya , 0] )
            index = index + 1
    NbPoints = index        
#    print("Nombre de points " , index ) 
#    print("---------------------------------------------------------------------------")
# calcul de la distance entre les points
    index = 0  
    for point1 in MatricePoint:
        for point2 in MatricePoint:
#            print( point1 )
#            print( point2) 
#            print( point1[0] , '-' ,point1[1]    )
            if point2 != point1:
                distance = math.sqrt( ( point1[1]-point2[1] )**2 + ( point1[2] -point2[2])**2  + (point1[3] - point2[3] )**2 )
#                print("distance ", point1[0],"-", point2[0] , " : ", distance )
                MatriceDistance.append( [ point1[0] ,point2[0] , distance ] )
                index = index + 1
#    print("--------------------------------------------------------------------------")
    TailleMatriceDistance = index
#    print("taille matrice des distances " , TailleMatriceDistance )
# Calcul des chemins possibles et de la distance parcourue
    distanceTotale = 0
    NbTrajet = 0
    MatriceCheminsTMP = [["p0",0 ,"p0"]]
    TailleMatriceCheminsTMP = 1
#==================Algo 2=============================================
    while TailleMatriceCheminsTMP < NbPoints:
        MatriceChemins[:] = []
        for chemin1 in MatriceCheminsTMP:
#            print('trt1 ', chemin1 )
            lastpoint = chemin1[2]
            for chemin2 in MatriceDistance:
#                print("trt2 : ", chemin2 )
                parcours = chemin1[0]
                if (lastpoint == chemin2[0]) and ( parcours.find(chemin2[1]) == -1 ):
                   parcours = parcours + "->" + chemin2[1]
#                   print(" Add ", chemin1[0] ,"+" , chemin2[2] , chemin2[0],"->",chemin2[1])
                   distanceTotale = chemin1[1] + chemin2[2]
                   MatriceChemins.append( [ parcours ,distanceTotale,chemin2[1]] )
        TailleMatriceCheminsTMP = TailleMatriceCheminsTMP +1
#        print( "Iteration generale n° " , TailleMatriceCheminsTMP )
        MatriceCheminsTMP[:] = []
        MatriceCheminsTMP = copy.deepcopy(MatriceChemins)
#        print( "--------------------------------------------------")
#    print( MatriceChemins )    
#    print("Mat.finale ", MatriceChemins)
#    print("==============================================================")

    distanceTotale = 99999999999999999999999.99  
    index = 0
    Resultat = ''
    for chemin1 in MatriceChemins:
#        print( chemin1[0] , chemin1[1] )
        if chemin1[1] < distanceTotale:
            Resultat = chemin1[0].split()
            distanceTotale = chemin1[1]
        index = index +1
#    print('Resultat : ' , Resultat ,'  pour une distance totale de ', distanceTotale , 'sur ', index ,' solutions possibles ')
        
           
    if (solution == Resultat):
           print("    OK: {}".format(solution))
    else:
            print(" >> KO: {} / {}".format(solution, Resultat))
