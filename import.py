import os
import os.path

exercices_in = os.listdir('./in')
exercices_out = os.listdir('./out')

if exercices_in != exercices_out:
    print("[ERROR] Exercices are missing either in the IN folder, either in the OUT folder : " + ', '.join(set(exercices_in).symmetric_difference(set(exercices_out))))
    exit()

for exercice_name in exercices_in:
    files_in = os.listdir('./in/'+exercice_name)
    files_out = os.listdir('./out/'+exercice_name)
    
    config_file = './config/'+exercice_name+'/'+'README.md'
    if not os.path.isfile(config_file):
        config_file = './config/'+exercice_name+'/'+'README_fr.md'
        if not os.path.isfile(config_file):
            print("[ERROR] No README (README.md/README_fr.md) for " + exercice_name)
            exit()
    
    config_file = open(config_file, 'r')
    readme = config_file.read().split('\n')
   
    if not len(readme) > 2:
        print("[ERROR] Readme must at least contains 3 lines, Short, Topic, and the reste of the text, for " + exercice_name)
        exit()
    if not readme[0].startswith('Short:'):
        print("[ERROR] Line 1 of readme must must start with 'Short:' for " + exercice_name)
        exit()
    if not readme[1].startswith('Topic:'):
        print("[ERROR] Line 2 of readme must must start with 'Topic:' for " + exercice_name)
        exit()
    
    no_extension_in = [i.replace('.in','') for i in files_in]
    no_extension_out = [i.replace('.out','') for i in files_out]
        
    if no_extension_in != no_extension_out:
        print("[ERROR] Test cases are missing either in the IN folder, either in the OUT folder of exercice " + exercice_name + " : " + ', '.join(set(no_extension_in).symmetric_difference(set(no_extension_out))))
        exit()

    extract = open(exercice_name + '.txt','w')
    
    test_case_list = []
    test_cases = []
    order = 1
    for file in no_extension_in:
        file_in = open('./in/'+exercice_name+'/'+file+'.in', 'r')
        file_out = open('./out/'+exercice_name+'/'+file+'.out', 'r')

        current = {
            "_id" : exercice_name+'-'+file,
            "_class" : "fr.nemolovich.apps.codeplatform.api.model.TestCase",
            "name" : file,
            "hidden" : "false" if 'public' in file else "true",
            "input" : file_in.read(),
            "output" : file_out.read(),
            "order" : int(order)
        }
        order += 1
        test_cases += [current]
    
        test_case_list += [{
            "$ref" : "codeplatform_testcase",
            "$id" : exercice_name+'-'+file
        }]
    
    current_exercice = {
        "_id" : exercice_name,
        "_class" : "fr.nemolovich.apps.codeplatform.api.model.Exercice",
        "title" : readme[0].replace('Short:','').strip(),
        "subtitle" : readme[1].replace('Topic:','').strip(),
        "text" : '\n'.join(readme[2:]),
        "type" : "FASTEST",
        "coeff" : "0.1",
        "testCases" : test_case_list,
        "codeStarters" : [],
        "codeHelpers" : []
    }
    
    extract.write("db.getCollection('codeplatform_exercice').insert(" + str(current_exercice) +")")
    extract.write('\n')
    extract.write("db.getCollection('codeplatform_testcase').insert(" + str(test_cases) +")")
    